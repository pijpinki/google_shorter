define({ "api": [
  {
    "type": "GET",
    "url": "/",
    "title": "Get short link",
    "group": "Main",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<ul> <li>access token</li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "apiKey",
            "description": "<ul> <li>Key for google shorter</li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "url",
            "description": "<ul> <li>url what need to short</li> </ul>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "link",
            "description": "<ul> <li>Shorten link</li> </ul>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     link: \"https://goo.gl/Sj6bHZ\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "object",
            "optional": false,
            "field": "error",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "error.code",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "error.message",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "Controllers/Index.js",
    "groupTitle": "Main",
    "name": "Get"
  }
] });
