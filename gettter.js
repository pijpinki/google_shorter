/**
 * For app.use
 * @param _class
 * @return {Function}
 */
module.exports = function (_class) {
    return function (req, res, next) {
        new _class(req, res, next)
    }
};