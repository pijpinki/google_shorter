const express = require('express');
const logger  = require('log4js').getLogger('App');
const config  = require('./config');
const getter  = require('./gettter');

const Index = require('./Controllers/Index');

const app    = express();
logger.level = 'all';

app.get(Index.endpoint, getter(Index));

app.listen(config.port, err => err ? logger.fatal(err) : logger.info('Server started at', config.port));

