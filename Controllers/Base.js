const log4js = require('log4js');

class Base {
    static get endpoint() {
        throw Error('Miss endpoint')
    }

    /**
     * Base handle constructor
     * @param {object} req - req from express
     * @param {object} res - res from express
     * @param {function } next - next from express
     */
    constructor(req, res, next) {
        this._req         = req;
        this._res         = res;
        this._next        = next;
        this.logger       = log4js.getLogger(this.constructor.endpoint);
        this.logger.level = 'all';

        this.data = Object.assign(this._req.query, this._req.body || {});

        this.onRequest();
    }

    /**
     * Request processor
     */
    onRequest() {
        throw Error('Miss onRequest');
        this;
    }

    /**
     * Send data
     * @param {*} data
     */
    send(data) {
        this._res.send(data)
    }

    /**
     * Send error
     * @param {(* | Error)} error
     */
    error(error) {
        this.logger.error(error, 'data ->', this.data);

        if (error instanceof Error)
            error = {code: 'Internal Server Error', message: error.message};

        this._res.status(500).send({error})
    }
}

module.exports = Base;