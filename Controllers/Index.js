const Base   = require('./Base');
const googl  = require('goo.gl');
const config = require('../config');

/**
 * @api {GET} / Get short link
 * @apiGroup Main
 * @apiParam {string} token - access token
 * @apiParam {string} apiKey - Key for google shorter
 * @apiParam {string} url - url what need to short
 *
 * @apiSuccess {string} link - Shorten link
 *
 * @apiError {object} error
 * @apiError {string} error.code
 * @apiError {string} error.message
 *
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          link: "https://goo.gl/Sj6bHZ"
 *     }
 */
class Index extends Base {
    static get endpoint() {
        return '/'
    }

    onRequest() {
        const {token, apiKey, url} = this.data;

        if (!token)
            return this.error(new Error('Miss token'));

        if (!apiKey)
            return this.error(new Error('Miss api key'));

        if (!url)
            return this.error(new Error('Miss url'));

        if (token !== config.accessToken)
            return this.error(new Error('Wrong access token'));

        const Googl = Object.assign(Object.create(Object.getPrototypeOf(googl)), googl);

        Googl.setKey(apiKey);
        Googl.shorten(url)
            .then((link) => this.send({link}))
            .catch((error) => this.error(error));
    }
}

module.exports = Index;