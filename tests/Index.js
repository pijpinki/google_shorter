const network = require('./libs/network');
const config  = require('../config');
const Index   = new network(`http://localhost:${config.port}/`, {logs: true});

const apiKey = config.testApiKey;

describe('Index test', function () {
    this.timeout(2 * 60 * 2000);

    it('Token error', clb => Index.errorTest('GET', {}, clb));
    it('Api Key error', clb => Index.errorTest('GET', {token: config.accessToken}, clb));
    it('Url Token', clb => Index.errorTest('GET', {token: config.accessToken, apiKey}, clb));
    it('Token success', clb => Index.successTest('GET', {token: config.accessToken, apiKey, url: 'хуй.ru'}, clb));
});