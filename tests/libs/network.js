const needle = require('needle')

class Network {
    /**
     * @constructor
     *
     * @param {string} url
     * @param {opts} [opts]
     */
    constructor(url, opts) {
        this.url = url

        this.opts = opts || {}
    }

    /**
     * Update Opts
     * @param opts
     */
    updateOpts(opts) {
        this.opts = Object.assign(this.opts, opts)
        return this
    }

    /**
     * Set opts
     * @param opts
     */
    setOpts(opts) {
        this.opts = opts
        return this
    }

    /**
     * Make success  test
     * @param {string} method GET, POST, PUT, DELETE
     * @param {object} data
     * @param {(opts | clb)} opts
     * @param {clb} [clb]
     */
    successTest(method, data, opts, clb) {
        if (clb)
            this.opts = Object.assign(this.opts, opts)

        if (typeof opts === 'function')
            [clb, opts] = [opts, {}]


        needle(method, this.url, this.makeData(data), (err, res, body) => {
            if (err)
                return clb(err)

            if (res.statusCode !== 200)
                return clb(body)

            if (this.opts.logs)
                console.log('body ->', body)

            clb(null, body)
        })
    }

    /**
     * Make error test
     * @param {string} method GET POST PUT DELETE
     * @param {*} data
     * @param {(opts | clb)} opts
     * @param {clb} [clb]
     */
    errorTest(method, data, opts, clb) {
        if (clb)
            this.opts = Object.assign(this.opts, opts)

        if (typeof opts === 'function')
            [clb, opts] = [opts, {}]

        needle(method, this.url, this.makeData(data), (err, res, body) => {
            if (err)
                return clb(err)

            if (res.statusCode === 200)
                return clb(body)

            if (this.opts.logs)
                console.log('body ->', body)

            clb(null, body)
        })
    }

    makeData(data) {
        if (this.opts.auth || !!this.opts.token)
            return Object.assign({token: this.opts.token}, data)

        return data
    }

    token(token) {
        this.opts.token = token
        return this
    }

    logOn() {
        this.opts.logs = true;
        return this
    }
}

module.exports = Network

/**
 * @callback clb
 * @param {Error} [error]
 * @param {*} [data]
 */

/**
 * @typedef {Object} opts
 * @property {boolean} [auth]
 * @property {boolean} [logs]
 * @property {*} [token]
 */
